<?php

namespace Nriman\Datasource;

use Nriman\Datasource\Utils;
use Nriman\Datasource\AggregateHelper;
class DataSourceLoader {
    public static function Load($dbSet, $params,$model) {
        $result = NULL;
        if (isset($dbSet) && get_class($dbSet) == "Nriman\Datasource\DbSet" && isset($params) && is_array($params)) {
            $dbSet->Select(Utils::GetItemValueOrDefault($params, "select"))
                  ->Filter(Utils::GetItemValueOrDefault($params, "filter"));
            
            $totalSummary = $dbSet->GetTotalSummary(Utils::GetItemValueOrDefault($params, "totalSummary"),
                                                    Utils::GetItemValueOrDefault($params, "filter"));
            if ($dbSet->GetLastError() !== NULL) {
                return $result;
            }
            
            if ($dbSet->GetLastError() !== NULL) {
                return $result;
            }
            $dbSet->Sort(Utils::GetItemValueOrDefault($params, "sort"));
            
            $groupCount = NULL;
            $skip = Utils::GetItemValueOrDefault($params, "skip");
            $take = Utils::GetItemValueOrDefault($params, "take");
            
            $result = array();
            if (isset($params["group"])) {
                $groupExpression = $params["group"];
                $groupSummary = Utils::GetItemValueOrDefault($params, "groupSummary");
                $dbSet->Group($groupExpression, $groupSummary, $skip, $take);    
                if (isset($params["requireGroupCount"]) && $params["requireGroupCount"] === true) {
                    $groupCount = $dbSet->GetGroupCount();
                }
                $result["data"] = $dbSet->AsArray();
            }
            else {
//                $dbSet->SkipTake($skip, $take);
                $data=new $model;
            
            
                if(isset($params['skip'])){
                    $data->skip($params['skip'])->take($params['take']);
                }
            
                if(isset($params['sort'])){
                    foreach ($params['sort'] as $item)
                        $data->orderBy($item->selector,$item->desc==false?'asc':'desc');
                }
                if(isset($params['filter'])){
                    $and=true;
                    foreach ($params['filter'] as $item){
                        if(is_array($item)){
                            switch ($item[1]){
                                case "contains": {
                                    $op = 'like';
                                    $val='%'.$item[2].'%';
                                break;
                                }
                                case "notcontains": {
                                    $op = "not like";
                                    $val='%'.$item[2].'%';
                                    break;
                                }
                                case "startswith": {
                                    $op="like";
                                    $val=$item[2];
                                        break;
                                }
                                case "endswith": {
                                    $op="like";
                                    $val='%'.$item[2];
                                        break;
                                }
                                default: {
                                    $op = $item[1];
                                    $val=$item[2];
                                }
                            }
                            if($and==true)
                                $data->where($item[0],$op,$val);
                            else
                                $data->orWhere($item[0],$op,$val);
                        }else{
                                $and=$item=='and'?true:false;
                        }
                    }
                }
                $result["data"] = $data->get();
            }
            if ($dbSet->GetLastError() !== NULL) {
                return $result;
            }
            $totalCount = (isset($params["requireTotalCount"]) && $params["requireTotalCount"] === true)? $data->count() : NULL;
            if (isset($totalCount)) {
                $result["totalCount"] = $totalCount;
            }
            if (isset($totalSummary)) {
                $result["summary"] = $totalSummary;
            }
            if (isset($groupCount)) {
                $result["groupCount"] = $groupCount;
            }
            $result['queryResult']=$dbSet->resultQuery;
        }
        else {
            throw new \Exception("Invalid params");
        }
        return $result;
    }
}
