<?php
namespace Nriman\Datasource;
use Nriman\Datasource\DataController;
//require_once("DataController.php");

class DataService{
    private $response;
    protected function GetParseParams($params, $assoc = false) {
        $result = NULL;
        if (is_array($params)) {
            $result = array();
            foreach ($params as $key => $value) {
                $result[$key] = json_decode($params[$key], $assoc);
                if ($result[$key] === NULL) {
                    $result[$key] = $params[$key];
                }
            }
        } 
        else {
            $result = $params;
        }
        return $result;
    }
    protected function GetParamsFromInput() {
        $result = NULL;
        $content = file_get_contents("php://input");
        if ($content !== false) {
            $params = array();
            parse_str($content, $params);
            $result = $this->GetParseParams($params, true); 
        } 
        return $result;
    }
    public function __construct($request,$model) {
        $this->response = NULL;
        $controller = new DataController($model);
        switch($request->method()) {
            case "GET": {
                $params = $this->GetParseParams($request->all());
                $this->response = $controller->Get($params); 
                break;
            }
            case "POST": {
                $params = $this->GetParamsFromInput();
                $this->response = $controller->Post($params["values"]); 
                break;
            }
            case "PUT": {
                $params = $this->GetParamsFromInput();
                $this->response = $controller->Put($params["key"], $params["values"]); 
                break;
            }
            case "DELETE": {
                $params = $this->GetParamsFromInput();       
                $this->response = $controller->Delete($params["key"]); 
                break;
            }

        }
        unset($controller);
    }
    public function getData() {
//        if (isset($this->response) && !is_string($this->response)) {
//            header("Content-type: application/json");
//            return json_encode($this->response,JSON_NUMERIC_CHECK );
//        }
//        else {
//            header("HTTP/1.1 500 Internal Server Error");
//            header("Content-Type: application/json");
//            return json_encode(array("message" => $this->response, "code" => 500));
//        }
        return response()->json($this->response, 200);
    }
    

}