<?php
//require_once("DevExtreme/LoadHelper.php");
namespace Nriman\Datasource;
use Nriman\Datasource\LoadHelper;
//spl_autoload_register(array("App\DevExtreme\LoadHelper", "LoadModule"));


use Nriman\Datasource\DbSet;
use Nriman\Datasource\DataSourceLoader;

class DataController {
    private $dbSet;
    private $model;
    public function __construct($model) {
        $this->model=$model;
        $model=new $model;
        $this->dbSet = new DbSet($model->getTable());
    }
    
    
    public function Get($params) {
        $result = DataSourceLoader::Load($this->dbSet, $params, $this->model);
        if (!isset($result)) {
            $result = $this->dbSet->GetLastError();
        }
        return $result;
    }
    public function Post($values) {
//        $result = $this->dbSet->Insert($values);
        $this->model::create($values);
        if (!isset($result)) {
            $result = $this->dbSet->GetLastError();
        }
        return $result;
    }
    public function Put($key, $values) {
        $result = NULL;
        if (isset($key) && isset($values) && is_array($values)) {
//            if (!is_array($key)) {
//                $keyVal = $key;
//                $key = array();
//                $key['id'] = $keyVal;
//            }
//            $result = $this->dbSet->Update($key, $values);
            $this->model::find($key)->update($values);
            if (!isset($result)) {
                $result = $this->dbSet->GetLastError();
            }
        }
        else {
            throw new Exeption("Invalid params");
        }
        return $result;
    }
    public function Delete($key) {
        $result = NULL;
        if (isset($key)) {
//            if (!is_array($key)) {
//                $keyVal = $key;
//                $key = array();
//                $key["id"] = $keyVal;
//            }
//            $result = $this->dbSet->Delete($key);
            $result = $this->model::find($key)->delete();
            print_r($key);
            if (!isset($result)) {
                $result = $this->dbSet->GetLastError();
            }
        }
        else {
            throw new Exeption("Invalid params");
        }
        return $result;
    }
}
